.PHONY: clean build-all

help:
	@echo "make [build-all | benchmark-all | show-versions | clean]"

build-all:
	cd Implementations/Java/Akka && make build
	cd Implementations/CSharp/Akka.NET && make build
	cd Implementations/Erlang && make build
	cd Implementations/Pony && make build
	cd Implementations/Rust/Actix && make build
	cd Implementations/Rust/Acteur && make build
	cd Implementations/Rust/Xactor && make build
	cd Implementations/Go && make build

benchmark-all:
	cd Benchmark && make benchmark plot

show-versions:
	@echo "Machine: " `uname -vm`
	@echo "-----------------------------------------------"
	@echo "C#:      " `pkg info | grep dotnet-runtime | cut -wf 1`
	@echo "Java:    " `pkg info | grep ^openjdk | cut -wf 1`
	@echo "Erlang:  " `pkg info | grep ^erlang-2 | cut -wf 1`
	@echo "Pony:    " `ponyc --version | head -n 1`
	@echo "Rust:    " `rustc --version`
	@echo "Go:      " `go version | cut -wf 3`

clean:
	cd Implementations/Java/Akka && make clean
	cd Implementations/CSharp/Akka.NET && make clean
	cd Implementations/Erlang && make clean
	cd Implementations/Pony && make clean
	cd Implementations/Rust/Actix && make clean
	cd Implementations/Rust/Acteur && make clean
	cd Implementations/Rust/Xactor && make clean
	cd Implementations/Go && make clean
	cd Benchmark && make clean
