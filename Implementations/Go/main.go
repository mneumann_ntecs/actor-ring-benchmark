package main

import (
	"fmt"
	"os"
	"strconv"
)

type Token struct {
	Count int
}

type Ring struct {
	N            int
	Term         <-chan int
	FirstChannel chan<- Token
}

func Node(id int, prev <-chan Token, next chan<- Token, term chan<- int) {
	for token := range prev {
		if token.Count > 0 {
			next <- Token{Count: token.Count - 1}
		} else {
			term <- id
		}
	}
}

func CreateRing(n int) Ring {
	term := make(chan int)

	firstChannel := make(chan Token)
	prevChannel := firstChannel

	for i := 1; i <= n; i += 1 {
		var nextChannel chan Token
		if i == n {
			nextChannel = firstChannel
		} else {
			nextChannel = make(chan Token)
		}
		go Node(i-1, prevChannel, nextChannel, term)
		prevChannel = nextChannel
	}

	return Ring{N: n, Term: term, FirstChannel: firstChannel}
}

func (ring *Ring) Cycle(count int) {
	ring.FirstChannel <- Token{Count: count}
	id := <-ring.Term
	fmt.Println("Token terminated at node", id)
}

func main() {
	ringSize, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic("Invalid ringSize")
	}

	numHops, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic("Invalid numHops")
	}

	fmt.Println("ringSize:", ringSize)
	fmt.Println("numHops:", numHops)

	ring := CreateRing(ringSize)
	ring.Cycle(numHops)
}
