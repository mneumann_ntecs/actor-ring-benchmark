package de.ntecs;

import akka.actor.typed.ActorSystem;

public class RingBenchmark {

  public static void main(String[] args) {
    int ringSize = Integer.parseInt(args[0]);
    int numHops = Integer.parseInt(args[1]);

    System.out.printf("ringSize=%d, numHops=%d\n", ringSize, numHops);

    final ActorSystem<Token> greeterMain = ActorSystem.create(
      RingActor.create(ringSize),
      "ring-benchmark"
    );

    greeterMain.tell(new Token(numHops));
  }
}
