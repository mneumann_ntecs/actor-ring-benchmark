package de.ntecs;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;

public class RingActor extends NodeActor {

  protected RingActor(ActorContext<Token> context, int ringSize) {
    super(context, 0, null);
    ActorRef<Token> currentNextNode = context.spawnAnonymous(
      NodeActor.create(ringSize - 1, context.getSelf())
    );
    for (int i = ringSize - 2; i > 0; i--) {
      currentNextNode =
        context.spawnAnonymous(NodeActor.create(i, currentNextNode));
    }

    this.setNextNode(currentNextNode);
  }

  public static Behavior<Token> create(int ringSize) {
    return Behaviors.setup(context -> new RingActor(context, ringSize));
  }
}
