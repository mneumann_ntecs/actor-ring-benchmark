package de.ntecs;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

public class NodeActor extends AbstractBehavior<Token> {
  final boolean DEBUG = false;

  private ActorRef<Token> nextNode;
  private int nodeId;

  @Override
  public Receive<Token> createReceive() {
    return newReceiveBuilder().onMessage(Token.class, this::onToken).build();
  }

  protected void setNextNode(ActorRef<Token> nextNode) {
    this.nextNode = nextNode;
  }

  protected NodeActor(
    ActorContext<Token> context,
    int nodeId,
    ActorRef<Token> nextNode
  ) {
    super(context);
    this.nodeId = nodeId;
    this.nextNode = nextNode;
  }

  private Behavior<Token> onToken(Token token) {
    if (DEBUG) {
      System.out.printf("Token: %d, Node: %d\n", token.GetCount(), this.nodeId);
    }
    if (token.Countdown()) {
      this.nextNode.tell(token);
    } else {
      if (DEBUG) {
        System.out.println("DONE");
      }
      this.getContext().getSystem().terminate();
    }

    return this;
  }

  public static Behavior<Token> create(int nodeId, ActorRef<Token> nextNode) {
    return Behaviors.setup(context -> new NodeActor(context, nodeId, nextNode));
  }
}
