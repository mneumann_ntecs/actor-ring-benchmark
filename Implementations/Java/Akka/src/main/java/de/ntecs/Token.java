package de.ntecs;

public class Token {
  private int count;

  public Token(int count) {
    this.count = count;
  }

  public int GetCount() {
    return this.count;
  }

  public boolean Countdown() {
    this.count -= 1;
    return this.count >= 0;
  }
}
