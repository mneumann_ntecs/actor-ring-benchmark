use "collections"

primitive Ring
  fun build(ringSize: I32, terminator: Terminator tag): Node tag =>
        // the last node will later be connected with the first node
        let lastNode = Node(None, terminator)
        var nextNode: Node tag = lastNode
        for _ in Range[I32](0, ringSize - 1) do
              nextNode = Node(nextNode, terminator)
        end
        // here, nextNode is the first node
        lastNode.setNextNode(nextNode)
        nextNode

