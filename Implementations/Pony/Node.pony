actor Node
  var _nextNode: (Node tag | None)
  let _terminator: Terminator tag

  new create(nextNode: (Node tag | None), terminator: Terminator tag) =>
    _nextNode = nextNode
    _terminator = terminator

  be forward(token: I64) =>
    if token > 0 then
      match _nextNode
      | let next: Node tag => next.forward(token - 1)
      end
    else
      _terminator.terminate()
    end

  be setNextNode(nextNode: Node tag) =>
    _nextNode = nextNode

