actor Main
  new create(env: Env) =>
        let shouldCallExit = try env.args(1)? == "exit" else false end
        let ringSize = try env.args(2)?.i32()? else 0 end
        let maxHops = try env.args(3)?.i64()? else 0 end

        env.out.print(if shouldCallExit then "exit" else "no exit" end)
        env.out.print("ring_size: " + ringSize.string())
        env.out.print("max_hops: " + maxHops.string())

        let terminator = Terminator(shouldCallExit)
        let ring = Ring.build(ringSize, terminator)
        ring.forward(maxHops)
