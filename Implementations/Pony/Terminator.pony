use "lib:c"

use @exit[None](status: I32)

actor Terminator
    let _shouldCallExit: Bool

    new create(shouldCallExit: Bool) =>
        _shouldCallExit = shouldCallExit

    be terminate() =>
        if _shouldCallExit then
                // HACK!!!
                // Pony's cycle collector, which is responsible for garbage collecting Actors,
                // is pretty bad at collecting a large, cyclic chain of Actors.
                // With a ring of 1 million actors, GC takes a long time, so just exit
                // when we are done.
                @exit(0)
        end

