﻿namespace RingBenchmark
{
    using System;
    using Akka.Actor;

    class Program
    {
        static void Main(string[] args)
        {
            AsyncMain(Int32.Parse(args[0]), Int32.Parse(args[1])).Wait();
        }

        private static async System.Threading.Tasks.Task AsyncMain(int ringSize, int numHops)
        {
            using (var system = ActorSystem.Create("ring-benchmark"))
            {
                var _benchmarkActor = system.ActorOf(Props.Create(() => new BenchmarkActor(() => system.Terminate(), ringSize, numHops)));

                await system.WhenTerminated;
            }
        }
    }

}