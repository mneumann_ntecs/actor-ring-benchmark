﻿namespace RingBenchmark
{
    using Akka.Actor;

    internal static class TokenForwarder
    {
        public static void ForwardToken(TokenMessage token, IActorRef nextNode, IActorRef reportWhenDoneActor)
        {
            if (token.Countdown())
            {
                nextNode.Tell(token);
            }
            else
            {
                reportWhenDoneActor.Tell(new DoneMessage());
            }
        }
    }
}
