﻿namespace RingBenchmark
{
    using Akka.Actor;

    internal class RingActor : ReceiveActor
    {
        public RingActor(int ringSize, IActorRef reportWhenDoneActor)
        {
            var currentNode = Context.ActorOf(Props.Create(() => new NodeActor(this.Self, reportWhenDoneActor)));
            for (int i = ringSize - 1; i > 0; i--)
            {
                currentNode = Context.ActorOf(Props.Create(() => new NodeActor(currentNode, reportWhenDoneActor)));
            }

            Receive<TokenMessage>(token => TokenForwarder.ForwardToken(token, currentNode, reportWhenDoneActor));
        }
    }
}