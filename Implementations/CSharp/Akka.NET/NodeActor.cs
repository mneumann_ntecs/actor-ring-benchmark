﻿namespace RingBenchmark
{
    using Akka.Actor;

    internal class NodeActor : ReceiveActor
    {
        public NodeActor(IActorRef nextNode, IActorRef reportWhenDoneActor)
        {
            Receive<TokenMessage>(token => TokenForwarder.ForwardToken(token, nextNode, reportWhenDoneActor));
        }
    }
}