﻿namespace RingBenchmark
{
    internal class TokenMessage
    {
        private int numHops;

        public TokenMessage(int numHops)
        {
            this.numHops = numHops;
        }

        public bool Countdown()
        {
            this.numHops -= 1;
            return this.numHops >= 0;
        }
    }
}