﻿namespace RingBenchmark
{
    using System;
    using System.Diagnostics;
    using Akka.Actor;

    internal class BenchmarkActor : ReceiveActor
    {
        public BenchmarkActor(Action terminate, int ringSize, int numHops)
        {
            var creationTime = new Stopwatch();
            creationTime.Start();
            var ringActor = Context.ActorOf(Props.Create(() => new RingActor(ringSize, this.Self)));
            creationTime.Stop();

            Console.WriteLine("ringSize: {0}", ringSize);
            Console.WriteLine("numHops: {0}", numHops);
            Console.WriteLine("creationTimeUs: {0}", creationTime.ElapsedMilliseconds * 1000);

            var messagingTime = new Stopwatch();
            messagingTime.Start();

            Receive<DoneMessage>(_ =>
            {
                messagingTime.Stop();
                Console.WriteLine("messagingTimeUs: {0}", messagingTime.ElapsedMilliseconds * 1000);
                terminate.Invoke();
            });

            ringActor.Tell(new TokenMessage(numHops));
        }
    }
}