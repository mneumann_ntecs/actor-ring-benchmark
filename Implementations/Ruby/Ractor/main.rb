# frozen_string_literal: true
#
# Each Ractor in Ruby 3.0 creates it's own thread. This means, this does not
# scale. 1000 threads are ok, but more will fail with `can't create Thread:
# Resource temporarily unavailable`

def run_node(node_id:, next_node:, exit_node:)
  loop do
    token = Ractor.receive
    if token > 0
      next_node.send(token - 1)
    else
      exit_node.send("stopped at #{node_id}")
    end
  end
end

def main(ring_size:, num_hops:)
  puts "ring_size: #{ring_size}"
  puts "num_hops: #{num_hops}"

  exit_node = Ractor.new do
    Ractor.receive
  end

  root_node = Ractor.new(exit_node) do |exit_node|
    run_node(node_id: 0, next_node: Ractor.receive, exit_node: exit_node)
  end

  current_node = root_node

  # Node n-1 .. 1
  for node_id in (ring_size-1).downto(1)
    current_node = Ractor.new(node_id, current_node, exit_node) do |node_id, next_node, exit_node|
      run_node(node_id: node_id, next_node: next_node, exit_node: exit_node)
    end
  end

  root_node.send(current_node)
  root_node.send(num_hops)

  puts exit_node.take
end

main(ring_size: Integer(ARGV[0]), num_hops: Integer(ARGV[1])) if __FILE__ == $0
