use tractor::*;

pub struct Node {
    id: usize,
    next_node: Option<Addr<Node>>,
    stopped_at: bool,
}

impl Node {
    fn new(id: usize) -> Self {
        Self {
            id,
            next_node: None,
            stopped_at: false,
        }
    }

    fn new_with_next(id: usize, next_node: Addr<Node>) -> Self {
        Self {
            id,
            next_node: Some(next_node),
            stopped_at: false,
        }
    }

    fn next_node(&self) -> &Addr<Node> {
        self.next_node.as_ref().unwrap()
    }
}

#[actor]
impl Node {
    fn token(&mut self, n: usize) {
        if n > 0 {
            self.next_node().token(n - 1);
        } else {
            println!("Stopped at node {}", self.id);
            self.stopped_at = true;
            self.next_node().stop_token();
            self.next_node = None; // Drop reference
        }
    }

    fn stop_token(&mut self) {
        // forward stop token
        if !self.stopped_at {
            self.next_node().stop_token();
        }
        self.next_node = None; // Drop reference
    }

    fn set_next_node(&mut self, next_node: Addr<Node>) {
        self.next_node = Some(next_node);
    }
}

fn run(ring_size: usize, num_hops: usize) {
    let first_node = Node::new(0).start();

    let mut next_node = first_node.clone();

    // ring_size-1 .. 1
    for i in 1..ring_size {
        next_node = Node::new_with_next(ring_size - i, next_node).start();
    }

    let () = first_node.set_next_node(next_node);
    let () = first_node.token(num_hops);
}

#[async_std::main]
async fn main() {
    let mut args = std::env::args();
    let _ = args.next();

    let ring_size: usize = args.next().unwrap().parse().unwrap();
    let num_hops: usize = args.next().unwrap().parse().unwrap();

    println!("ring_size: {:?}", ring_size);
    println!("num_hops: {:?}", num_hops);

    run(ring_size, num_hops);

    ActorSystem::wait_for_completion_async().await;
}
