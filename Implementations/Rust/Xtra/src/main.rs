use async_trait::async_trait;
use xtra::{prelude::*, spawn::AsyncStd};

// Messages

struct Token(pub usize);
impl Message for Token {
    type Result = ();
}

struct StopToken;
impl Message for StopToken {
    type Result = ();
}

struct SetNextNode(Address<Node>);
impl Message for SetNextNode {
    type Result = ();
}

// Actors

struct Node {
    next_node: Option<Address<Node>>,
    stopped_at: bool,
}

impl Node {
    fn new(_id: usize) -> Self {
        Self {
            next_node: None,
            stopped_at: false,
        }
    }

    fn new_with_next(_id: usize, next_node: Address<Node>) -> Self {
        Self {
            next_node: Some(next_node),
            stopped_at: false,
        }
    }

    fn next_node(&self) -> &Address<Node> {
        self.next_node.as_ref().unwrap()
    }
}

impl Actor for Node {}

#[async_trait]
impl Handler<SetNextNode> for Node {
    async fn handle(&mut self, message: SetNextNode, _ctx: &mut Context<Self>) {
        self.next_node = Some(message.0);
    }
}

#[async_trait]
impl Handler<Token> for Node {
    async fn handle(&mut self, message: Token, _ctx: &mut Context<Self>) {
        match message {
            Token(n) if n > 0 => {
                let () = self.next_node().do_send_async(Token(n - 1)).await.unwrap();
            }
            Token(n) => {
                debug_assert!(n == 0);
                self.stopped_at = true;
                let () = self.next_node().do_send_async(StopToken).await.unwrap();
            }
        }
    }
}

#[async_trait::async_trait]
impl Handler<StopToken> for Node {
    async fn handle(&mut self, _message: StopToken, ctx: &mut Context<Self>) {
        if !self.stopped_at {
            let () = self
                .next_node
                .as_ref()
                .unwrap()
                .do_send_async(StopToken)
                .await
                .unwrap();
        }
        ctx.stop();
    }
}

#[async_std::main]
async fn main() {
    let mut args = std::env::args();
    let _ = args.next();

    let ring_size: usize = args.next().unwrap().parse().unwrap();
    let num_hops: usize = args.next().unwrap().parse().unwrap();

    println!("ring_size: {:?}", ring_size);
    println!("num_hops: {:?}", num_hops);

    let first_node = Node::new(0).create(None).spawn(&mut AsyncStd);

    let mut next_node = first_node.clone();

    // ring_size-1 .. 1
    for i in 1..ring_size {
        next_node = Node::new_with_next(ring_size - i, next_node)
            .create(None)
            .spawn(&mut AsyncStd);
    }

    let () = first_node.send(SetNextNode(next_node)).await.unwrap();
    let () = first_node.do_send_async(Token(num_hops)).await.unwrap();

    while first_node.is_connected() {
        async_std::task::sleep(std::time::Duration::from_millis(250)).await;
    }
}
