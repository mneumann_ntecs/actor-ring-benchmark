#[macro_use]
extern crate log;

use riker::actors::*;

#[derive(Debug, Clone)]
enum NodeMsg {
    Token(usize),
    Connect(BasicActorRef),
}

#[derive(Debug)]
struct Node {
    next_node: Option<BasicActorRef>,
}

impl Default for Node {
    fn default() -> Self {
        Self { next_node: None }
    }
}

impl Actor for Node {
    type Msg = NodeMsg;

    fn recv(&mut self, ctx: &Context<NodeMsg>, msg: NodeMsg, _sender: Sender) {
        match msg {
            NodeMsg::Token(n) if n > 0 => match self.next_node {
                Some(ref node) => {
                    node.try_tell(NodeMsg::Token(n - 1), None).unwrap();
                }
                None => {
                    panic!("Not connected");
                }
            },
            NodeMsg::Token(_) => {
                // XXX: await
                ctx.system.shutdown();
            }
            NodeMsg::Connect(next) => {
                debug!("Got connect msg");
                self.next_node = Some(next);
            }
        }
    }
}

fn main() {
    let mut args = std::env::args();
    let _ = args.next();

    let ring_size: usize = args.next().unwrap().parse().unwrap();
    let num_hops: usize = args.next().unwrap().parse().unwrap();

    println!("ring_size: {:?}", ring_size);
    println!("num_hops: {:?}", num_hops);

    let system = ActorSystem::with_name("ring").unwrap();

    let nodes: Result<Vec<_>, _> = (0..ring_size)
        .into_iter()
        .map(|i| system.actor_of::<Node>(&format!("node{}", i)))
        .collect();
    let nodes = nodes.unwrap();

    for i in 0..ring_size {
        let next_node: ActorRef<NodeMsg> = nodes[(i + 1) % ring_size].clone();
        nodes[i].tell(NodeMsg::Connect(next_node.into()), None);
    }

    let () = nodes[0].tell(NodeMsg::Token(num_hops), None);

    // XXX:
    std::thread::sleep(std::time::Duration::from_millis(500));
}
