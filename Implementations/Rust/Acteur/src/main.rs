use acteur::{Acteur, Actor, ActorAssistant, Receive};
use async_trait::async_trait;

#[derive(Debug)]
struct Node {
    id: u32,
    next_node: u32,
}

#[async_trait]
impl Actor for Node {
    type Id = u32;

    async fn activate(id: Self::Id, _: &ActorAssistant<Self>) -> Self {
        Self {
            id,
            next_node: id + 1,
        }
    }
}

#[derive(Debug)]
struct SetNextNode(u32);

#[async_trait]
impl Receive<SetNextNode> for Node {
    async fn handle(&mut self, message: SetNextNode, _: &ActorAssistant<Self>) {
        self.next_node = message.0;
    }
}

#[derive(Debug)]
struct Token(pub usize);

#[async_trait]
impl Receive<Token> for Node {
    async fn handle(&mut self, message: Token, assistant: &ActorAssistant<Self>) {
        match message {
            Token(n) if n > 0 => {
                assistant
                    .send_to_actor::<Node, Token>(self.next_node, Token(n - 1))
                    .await;
            }
            _ => {
                println!("stopped at node: {}", self.id);
                assistant.stop_system();
            }
        }
    }
}

fn main() {
    let mut args = std::env::args();
    let _ = args.next();

    let ring_size: u32 = args.next().unwrap().parse().unwrap();
    let num_hops: usize = args.next().unwrap().parse().unwrap();

    println!("ring_size: {:?}", ring_size);
    println!("num_hops: {:?}", num_hops);

    let sys = Acteur::new();

    sys.send_to_actor_sync::<Node, SetNextNode>(ring_size - 1, SetNextNode(0));

    sys.send_to_actor_sync::<Node, Token>(0, Token(num_hops));

    sys.wait_until_stopped();
}
