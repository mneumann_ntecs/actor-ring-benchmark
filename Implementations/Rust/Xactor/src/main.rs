use xactor::*;

#[message(result = "()")]
struct Token(pub usize);

#[message(result = "()")]
struct StopToken;

#[message(result = "()")]
struct SetNextNode(Addr<Node>);

struct Node {
    next_node: Option<Addr<Node>>,
    stopped_at: bool,
}

impl Node {
    fn new(_id: usize) -> Self {
        Self {
            next_node: None,
            stopped_at: false,
        }
    }

    fn new_with_next(_id: usize, next_node: Addr<Node>) -> Self {
        Self {
            next_node: Some(next_node),
            stopped_at: false,
        }
    }

    fn next_node(&self) -> &Addr<Node> {
        self.next_node.as_ref().unwrap()
    }
}

impl Actor for Node {}

#[async_trait::async_trait]
impl Handler<SetNextNode> for Node {
    async fn handle(&mut self, _ctx: &mut Context<Self>, message: SetNextNode) {
        self.next_node = Some(message.0);
    }
}

#[async_trait::async_trait]
impl Handler<Token> for Node {
    async fn handle(&mut self, _ctx: &mut Context<Self>, message: Token) {
        match message {
            Token(n) if n > 0 => {
                let () = self.next_node().send(Token(n - 1)).unwrap();
            }
            Token(n) => {
                debug_assert!(n == 0);
                self.stopped_at = true;
                let () = self.next_node().send(StopToken).unwrap();
            }
        }
    }
}

#[async_trait::async_trait]
impl Handler<StopToken> for Node {
    async fn handle(&mut self, ctx: &mut Context<Self>, _message: StopToken) {
        if !self.stopped_at {
            let () = self.next_node.as_ref().unwrap().send(StopToken).unwrap();
        }
        ctx.stop(None);
    }
}

#[xactor::main]
async fn main() -> Result<()> {
    let mut args = std::env::args();
    let _ = args.next();

    let ring_size: usize = args.next().unwrap().parse().unwrap();
    let num_hops: usize = args.next().unwrap().parse().unwrap();

    println!("ring_size: {:?}", ring_size);
    println!("num_hops: {:?}", num_hops);

    let first_node = Node::new(0).start().await?;

    let mut next_node = first_node.clone();

    // ring_size-1 .. 1
    for i in 1..ring_size {
        next_node = Node::new_with_next(ring_size - i, next_node)
            .start()
            .await?;
    }

    let () = first_node.call(SetNextNode(next_node)).await?;

    let () = first_node.send(Token(num_hops))?;

    first_node.wait_for_stop().await;

    Ok(())
}
