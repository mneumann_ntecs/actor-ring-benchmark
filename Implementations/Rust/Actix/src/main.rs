use actix::prelude::*;

enum RingOrNode {
    Ring(Addr<RingActor>),
    Node(Addr<NodeActor>),
}

struct NodeActor {
    next_node: RingOrNode,
}

impl Actor for NodeActor {
    type Context = Context<Self>;
}

struct RingActor {
    first_node: Option<Addr<NodeActor>>,
}

impl Actor for RingActor {
    type Context = Context<Self>;
}

#[derive(Debug)]
struct Token(pub usize);

struct SetFirstNode {
    pub first_node: Addr<NodeActor>,
}

impl Message for SetFirstNode {
    type Result = ();
}

impl Message for Token {
    type Result = ();
}

impl Handler<Token> for NodeActor {
    type Result = ();

    fn handle(&mut self, msg: Token, _ctx: &mut Context<Self>) -> Self::Result {
        if msg.0 > 0 {
            match self.next_node {
                RingOrNode::Node(ref node) => {
                    node.do_send(Token(msg.0 - 1));
                }
                RingOrNode::Ring(ref ring) => {
                    ring.do_send(Token(msg.0 - 1));
                }
            }
        } else {
            System::current().stop();
        }
    }
}

impl Handler<Token> for RingActor {
    type Result = ();

    fn handle(&mut self, msg: Token, _ctx: &mut Context<Self>) -> Self::Result {
        if msg.0 > 0 {
            match self.first_node {
                Some(ref node) => {
                    node.do_send(Token(msg.0 - 1));
                }
                None => {
                    panic!();
                }
            }
        } else {
            System::current().stop();
        }
    }
}

impl Handler<SetFirstNode> for RingActor {
    type Result = ();

    fn handle(&mut self, msg: SetFirstNode, _ctx: &mut Context<Self>) -> Self::Result {
        self.first_node = Some(msg.first_node);
    }
}

fn main() -> std::io::Result<()> {
    let mut args = std::env::args();
    let _ = args.next();

    let ring_size: usize = args.next().unwrap().parse().unwrap();
    let num_hops: usize = args.next().unwrap().parse().unwrap();

    println!("ring_size: {:?}", ring_size);
    println!("num_hops: {:?}", num_hops);

    let system = System::new("ring-benchmark");

    let ring = RingActor { first_node: None }.start();

    let mut current_node = NodeActor {
        next_node: RingOrNode::Ring(ring.clone()),
    }
    .start();

    for _i in 1..ring_size {
        current_node = NodeActor {
            next_node: RingOrNode::Node(current_node),
        }
        .start();
    }

    ring.do_send(SetFirstNode {
        first_node: current_node,
    });
    ring.do_send(Token(num_hops));

    system.run()
}
