-module(measure).
-export([main/1]).

main([RingSizeStr, NumHopsStr]) ->
        RingSize = list_to_integer(RingSizeStr),
        NumHops = list_to_integer(NumHopsStr),
        io:format("~w~n", [erlang:memory()]),
        {C1, _} = erlang:statistics(context_switches),
        T1 = erlang:timestamp(),
        Ring = ring:create(RingSize, self()),
        T2 = erlang:timestamp(),
        ring:feed(Ring, NumHops),
        receive {done, _} -> nil end,
        io:format("~w~n", [erlang:memory()]),
        T3 = erlang:timestamp(),
        {C2, _} = erlang:statistics(context_switches),

        io:format("{ringSize: ~.10B"
                  ",numHops: ~.10B"
                  ",creationTimeUs: ~.10B"
                  ",messagingTimeUs: ~.10B" 
                  ",statContextSwitches: ~.10B" 
                  "}~n", [
                   RingSize,
                   NumHops,
                   timer:now_diff(T2, T1),
                   timer:now_diff(T3, T2),
                   C2 - C1
                   ]).
        
