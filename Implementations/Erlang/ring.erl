-module(ring).

-export([create/2, feed/2]).

% Ring creation occurs from right to left:
%
%   [Node N-1] -> [Node 2] -> [Node 1] -> [Node 0]
%    ^
%    \---- Tail
%
create(RingSize, NotifyWhenDonePid) when RingSize >= 1 ->
        spawn(fun() ->
          Node0 = self(),
          Tail = create(RingSize - 1, Node0, NotifyWhenDonePid),
          loop(0, Tail, NotifyWhenDonePid)
        end).

create(0, NextNode, _) ->
        NextNode;
create(N, NextNode, NotifyWhenDonePid) ->
        Node = spawn_node(N, NextNode, NotifyWhenDonePid),
        create(N - 1, Node, NotifyWhenDonePid).

spawn_node(Id, NextNode, NotifyWhenDonePid) ->
        spawn(fun() -> loop(Id, NextNode, NotifyWhenDonePid) end).

loop(Id, NextNode, NotifyWhenDonePid) ->
        receive
                {message, 0} ->
                        io:fwrite("Stopped at node: ~w.~n", [Id]),
                        NotifyWhenDonePid ! {done, self()};
                {message, NumHops} ->
                        NextNode ! {message, NumHops - 1}
        end,
        loop(Id, NextNode, NotifyWhenDonePid).

feed(StartNode, NumHops) ->
        StartNode ! {message, NumHops}.
