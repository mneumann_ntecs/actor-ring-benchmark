import json
import glob
import matplotlib.pyplot as plt
import numpy as np
import fmt_utils

def select_field(field):
  return lambda e: e[field]

def fst(t):
  return t[0]

def snd(t):
  return t[1]

def plot_memory_usage(logs, field='rss_mib', ylabel='Real Memory [MiB]'):
  #plt.figure(figsize=(9,3))

  for log in logs:
    label = log['benchmark']['title']
    measurements = log['measurements']
    data = list(map(select_field(field), measurements))
    elapsed = list(map(select_field('elapsed_time'), measurements))
    plt.plot(elapsed, data, label=label)

  plt.grid(True, which="both", linestyle="dotted")
  plt.xlabel("Time [s]", fontsize=7)
  plt.ylabel(ylabel, fontsize=7)
  plt.xticks(fontsize=7)
  plt.yticks(fontsize=7)
  #mp.legend(loc='upper left', fancybox=True, shadow=True, prop=dict(size=8))
  plt.savefig("figure.png")
  # print(list(vss), list(rss))


def plot_total_runtime_barplot(logs, name, title):
  pairs = sorted(list(map(lambda log: (log['benchmark']['name'], log['total_runtime']), logs)), key=snd)
  labels = list(map(fst, pairs))
  values = list(map(snd, pairs))
  y_pos = list(map(fst, enumerate(pairs)))

  colors = plt.get_cmap('RdYlGn')(np.linspace(0.85, 0.15, len(pairs)))

  plt.figure(figsize=(16,9))
  plt.gca().invert_yaxis()
  for y, value, color in zip(y_pos, values, colors):
    runtime = value # factor = value / values[0]
    factor = value / values[0]
    plt.barh(y=y, width=value, align='center', alpha=1.0, color=color)
    #plt.text(x=value, y=y, s=" {:.1f}s {:.1f}x".format(runtime, factor), ha='left', va='top')
    plt.text(x=value, y=y, s=" {:.1f} x".format(factor), ha='left', va='center')

  plt.yticks(y_pos, labels, fontsize=10)
  plt.tick_params(axis="y", length=0)
  plt.xlabel('Runtime [s]')
  plt.title(title)
  plt.grid(True, which="both", axis="both", linestyle="dotted") # x
  #plt.subplots_adjust(left=0.1, right=0.9)
  plt.savefig(name)

def load_log(path):
  with open(path) as f:
    return json.loads(f.read())
 
def load_all_logs(pattern):
  return map(load_log, glob.glob(pattern))

def group_logs_by_configuration(logs):
  groups = {}
  for log in logs:
    conf = log['configuration']
    key = (conf['actors'], conf['hops'])
    if not key in groups:
      groups[key] = []
    groups[key].append(log)
  return groups

logs = list(load_all_logs("./Logs/*.json"))
groups = group_logs_by_configuration(logs)

for key in groups.keys():
  print(key)
  group = groups[key]
  actors, hops = key
  plot_total_runtime_barplot(
    group,
    name="{:d}_{:d}.png".format(actors, hops),
    title="{:s}, {:s}".format(fmt_utils.pluralize_human(actors, "Actor"), fmt_utils.pluralize_human(hops, "Hop")))

#plot_memory_usage(logs)
