def num_to_human(n):
  k = n // 1000
  m = k // 1000

  if m > 0:
    assert(n % 1000000 == 0)
    return "{:d}m".format(m)
  elif k > 0:
    assert(n % 1000 == 0)
    return "{:d}k".format(k)
  else:
    return "{:d}".format(n)

assert(num_to_human(1) == "1")
assert(num_to_human(1000) == "1k")
assert(num_to_human(10 * 1000) == "10k")
assert(num_to_human(100 * 1000) == "100k")
assert(num_to_human(1000 * 1000) == "1m")
assert(num_to_human(10 * 1000 * 1000) == "10m")
assert(num_to_human(100 * 1000 * 1000) == "100m")

def pluralize(n, what):
  return "{:d} {:s}{:s}".format(n, what, "s" if n > 1 else "")

def pluralize_human(n, what):
  return "{:s} {:s}{:s}".format(num_to_human(n), what, "s" if n > 1 else "")

