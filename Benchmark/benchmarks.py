import versions

CMD_ERL = "/usr/local/bin/erl"
CMD_DOTNET = "/usr/local/bin/dotnet"

class Benchmark:
  def __init__(self, name, title, path, command, lang, condition=None):
    self.name = name
    self.title = title
    self.path = path
    self.command = command
    self.lang = lang
    self.condition = condition

  def supports_configuration(self, configuration):
    if self.condition == None:
      return True
    return self.condition(configuration)

def limit_actors(limit):
  return lambda conf: conf.actors <= limit

V = versions.Versions()

ALL = [
  Benchmark(
    name = "erlang-default",
    title = "Erlang",
    path = ["Erlang"],
    command = lambda conf: [CMD_ERL, "-noshell", "-run", "measure", "main", str(conf.actors), str(conf.hops), "-s", "init", "stop"],
    lang = V.erlang,
    condition = limit_actors(230 * 1000)),

  Benchmark(
    name = "erlang-1mio",
    title = "Erlang (+P 1000000)",
    path = ["Erlang"],
    command = lambda conf: [CMD_ERL, "-noshell", "+P", "1000100", "-run", "measure", "main", str(conf.actors), str(conf.hops), "-s", "init", "stop"],
    lang = V.erlang,
    condition = limit_actors(1000 * 1000)),

  Benchmark(
    name = "pony-default",
    title = "Pony",
    path = ["Pony"],
    command = lambda conf: ["./benchmark", "default", str(conf.actors), str(conf.hops)],
    lang = V.pony,
    condition = limit_actors(750 * 1000)),

  Benchmark(
    name = "pony-with-exit",
    title = "Pony (w/ exit)",
    path = ["Pony"],
    command = lambda conf: ["./benchmark", "exit", str(conf.actors), str(conf.hops)],
    lang = V.pony,
    condition = limit_actors(1000 * 1000)),

  Benchmark(
    name = "pony-with-exit-noblock",
    title = "Pony (w/ exit, --ponynoblock)",
    path = ["Pony"],
    command = lambda conf: ["./benchmark", "--ponynoblock", "exit", str(conf.actors), str(conf.hops)],
    lang = V.pony,
    condition = limit_actors(1000 * 1000)),

  Benchmark(
    name = "akka-net-default",
    title = "C# Akka.NET",
    path = ["CSharp", "Akka.NET"],
    command = lambda conf: [CMD_DOTNET, "run", "-c", "Release", "--no-build", "--", str(conf.actors), str(conf.hops)],
    lang = V.dotnet,
    condition = limit_actors(1000 * 1000)),

  Benchmark(
    name = "akka-java-default",
    title = "Java Akka",
    path = ["Java", "Akka"],
    command = lambda conf: ["./build/install/Akka/bin/Akka", str(conf.actors), str(conf.hops)],
    lang = V.jdk,
    condition = limit_actors(1000 * 1000)),

  Benchmark(
    name = "rust-actix-default",
    title = "Rust Actix",
    path = ["Rust", "Actix"],
    command = lambda conf: ["./target/release/ring-benchmark-actix", str(conf.actors), str(conf.hops)],
    lang = V.rust,
    condition = limit_actors(10 * 1000 * 1000)),

  Benchmark(
    name = "rust-acteur-default",
    title = "Rust Acteur",
    path = ["Rust", "Acteur"],
    command = lambda conf: ["./target/release/ring-benchmark-acteur", str(conf.actors), str(conf.hops)],
    lang = V.rust,
    condition = limit_actors(10 * 1000 * 1000)),

  Benchmark(
    name = "rust-xactor-default",
    title = "Rust Xactor",
    path = ["Rust", "Xactor"],
    command = lambda conf: ["./target/release/ring-benchmark-xactor", str(conf.actors), str(conf.hops)],
    lang = V.rust,
    condition = limit_actors(10 * 1000 * 1000)),

  Benchmark(
    name = "go-default",
    title = "Go",
    path = ["Go"],
    command = lambda conf: ["./benchmark", str(conf.actors), str(conf.hops)],
    lang = V.go,
    condition = limit_actors(5 * 1000 * 1000)),
]
