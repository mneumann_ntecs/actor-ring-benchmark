require 'optparse'
require 'yaml'
require 'pp'

COLUMN_RUNTIME = 0
COLUMN_REAL_MEMORY = 2

class String
        def single_quote
                "'" + self + "'"
        end
end

class Array
        def avg
                self.sum / self.size
        end
end

def num_to_human(n)
  k = n / 1000
  m = k / 1_000

  if m > 0
   raise if n % 1_000_000 != 0
    "#{m}m" 
  elsif k > 0
    raise if n % 1000 != 0
    "#{k}k"
  else
    "#{n}"
  end
end

raise unless num_to_human(1) == "1"
raise unless num_to_human(1000) == "1k"
raise unless num_to_human(10_000) == "10k"
raise unless num_to_human(100_000) == "100k"
raise unless num_to_human(1000_000) == "1m"
raise unless num_to_human(10_000_000) == "10m"
raise unless num_to_human(100_000_000) == "100m"


def run_benchmark(benchmark, params, force = true)
        logfile = logfile_for_benchmark(benchmark, params)
        return false if File.exists?(logfile) and !force
        p benchmark, params
        system( "./measure.sh",
                benchmark[:impl],
                benchmark[:variant],
                params[:actors].to_s,
                params[:hops].to_s,
                logfile)
        if File.readlines(logfile).size < 2
		puts "Fixing log #{logfile}"
                File.open(logfile, 'a') {|f| f.puts "0.0\t0.0\t0.0\t0.0" }
	end
        return true
end

def parse_logfile(logfile)
        columns = []
        File.readlines(logfile).each{|line|
                next if line.start_with?("#")
                line.split.each.with_index {|value, col|
                        (columns[col] ||= []) << value.to_f
                }
        }
        columns
end

def gen_plot(params, selected_benchmarks, max_y_range, group_idx, groups_size, out)
        group_str = "(Group #{group_idx + 1} / #{groups_size})"
        group_file_str = "_group_#{group_idx + 1}"
        output_file = File.join($plot_dir, "ring_#{num_to_human(params[:actors])}_#{num_to_human(params[:hops])}#{group_file_str}.png")
        out.puts "set terminal png"
        out.puts "set style data line"
        out.puts "set title '#{num_to_human(params[:actors])} Actors / #{num_to_human(params[:hops])} Messages #{group_str}'"
        out.puts "set output '#{output_file}'"
        out.puts "set xlabel 'Time [s]'"
        out.puts "set ylabel 'Memory (Real) [MiB]'"
        out.puts "set xtic auto"
        out.puts "set ytic auto"
        out.puts "set grid"
        out.puts "show grid"
        out.puts "set yrange [0:#{max_y_range}]" if max_y_range and max_y_range > 0.0
        out.puts "set key box"
        out.puts("plot " + selected_benchmarks.map do |info|
                [info[:logfile].single_quote, "using", "1:3",
                "title", info[:benchmark][:title].single_quote,
                "lw", "2"].join(" ")
        end.join(", "))
end

def logfile_for_benchmark(benchmark, params)
  File.absolute_path(
      File.join($log_dir,
                [
                  benchmark[:name],
                  num_to_human(params[:actors]),
                  num_to_human(params[:hops])
                ].join("_") + ".log"))
end

def cluster(infos, span)
        groups = []

        while not infos.empty?
                min_runtime = [infos.map{|info| info[:max_runtime]}.min || 0.0, 2.0].max
                a, b = infos.partition {|info| info[:max_runtime] <= min_runtime * span}
                groups << a unless a.empty?
                infos = b
        end
        return groups
end


def plot_all(benchmarks, all_params)
    all_params.each do |params|

        selected_benchmarks = benchmarks.select do |benchmark|
                limit = benchmark[:limit_actors]
                limit.nil? or params[:actors] <= Integer(limit)
        end

        infos = selected_benchmarks.map do |benchmark|
            logfile = logfile_for_benchmark(benchmark, params)
            columns = parse_logfile(logfile)
            {
                benchmark: benchmark,
                logfile: logfile,
                params: params,
                max_runtime: (columns[COLUMN_RUNTIME] || []).max || 0.0,
                max_real_memory: (columns[COLUMN_REAL_MEMORY] || []).max || 0.0,
            }
        end

        groups = cluster(infos, 2.5)
        groups.each.with_index do |group, group_idx|
            max_y_range = group.map {|info| info[:max_real_memory]}.max || 0.0
            IO.popen("gnuplot", "w+", :err => '/dev/null') do |io|
                    gen_plot(params, group, max_y_range * (1.0 + 0.1 * group.size), group_idx, groups.size, io)
                    io.close_write
            end
        end
    end
end

def run_benchmarks(benchmarks, all_params, force = true)
    all_params.each do |params|

        selected_benchmarks = benchmarks.select do |benchmark|
                limit = benchmark[:limit_actors]
                limit.nil? or params[:actors] <= Integer(limit)
        end

        selected_benchmarks.each do |benchmark|
            run_benchmark(benchmark, params, force)
        end
    end
end

# convert keys of hash to symbols (they come in as String from YAML)
def map_hash_keys_to_symbols_recursive(obj)
  if obj.is_a?(Hash)
    h = {}
    obj.each {|k,v| h[k.to_sym] = map_hash_keys_to_symbols_recursive(v) }
    h
  elsif obj.is_a?(Array)
    obj.map{|v| map_hash_keys_to_symbols_recursive(v)}
  else
    obj
  end
end

def load_config(file)
  config = YAML.load(File.read(file))
  map_hash_keys_to_symbols_recursive(config)
end

if __FILE__ == $0
  params = {force: false, "log-dir": "./Logs", "plot-dir": "./Plots", config: "config.yaml", select: nil, reject: nil}
  OptionParser.new do |opts|
    opts.on('--force')
    opts.on('--log-dir=PATH', String)
    opts.on('--plot-dir=PATH', String)
    opts.on('--select=PATTERN', String)
    opts.on('--reject=PATTERN', String)
    opts.on('--plot')
    opts.on('--benchmark')
    opts.on('--config=YAML', String)
  end.parse!(into: params)

  $log_dir = params[:"log-dir"]
  $plot_dir = params[:"plot-dir"]

  if params[:plot].nil? and params[:benchmark].nil?
    raise "Neither --plot nor --benchmark provided"
  end

  config = load_config(params[:config])
  pp config

  benchmarks = config[:benchmarks]
  benchmarks = if select_pattern = params[:select]
    select_regexp = Regexp.new(select_pattern)
    benchmarks.select {|b| select_regexp =~ b[:name].downcase}
  else
    benchmarks
  end
  benchmarks = if reject_pattern = params[:reject]
    reject_regexp = Regexp.new(reject_pattern)
    benchmarks.reject {|b| reject_regexp =~ b[:name].downcase}
  else
    benchmarks
  end

  pp benchmarks

  if params[:benchmark]
    run_benchmarks(benchmarks, config[:params], params[:force]) 
  end

  if params[:plot]
    plot_all(benchmarks, config[:params])
  end
end
