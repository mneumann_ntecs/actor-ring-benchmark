import process_util

class Versions:
  def __init__(self):
    self.dotnet = "dotnet " + process_util.execute_cmd("dotnet --version").rstrip()
    self.jdk = process_util.execute_cmd("pkg info | grep ^openjdk | cut -wf 1").rstrip()
    self.erlang = process_util.execute_cmd("pkg info | grep ^erlang-2 | cut -wf 1").rstrip()
    self.pony = "ponyc " + process_util.execute_cmd("ponyc --version").split()[0]
    self.rust = process_util.execute_cmd("rustc --version").rstrip()
    self.go = process_util.execute_cmd("go version | cut -wf 3").split()[0]

  def as_dict(self):
    return {
      'dotnet': self.dotnet,
      'jdk': self.jdk,
      'erlang': self.erlang,
      'pony': self.pony,
      'rust': self.rust,
      'go': self.go,
    }
