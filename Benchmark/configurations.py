class Configuration:
  def __init__(self, actors, hops):
    self.actors = actors
    self.hops = hops

  def as_dict(self):
    return {'actors': self.actors, 'hops': self.hops}

  def __repr__(self):
    return "Configuration(actors={:d}, hops={:d})".format(self.actors, self.hops)

ALL = [
  # Benchmark system startup time
  Configuration(actors = 10, hops = 1),

  # Benchmark actor creation
  Configuration(actors = 1000, hops = 1),
  Configuration(actors = 10 * 1000, hops = 1),
  Configuration(actors = 100 * 1000, hops = 1),
  Configuration(actors = 1000 * 1000, hops = 1),
  Configuration(actors = 10 * 1000 * 1000, hops = 1),

  # Benchmark messaging performance
  Configuration(actors = 10, hops = 10 * 1000 * 1000),
  Configuration(actors = 1000, hops = 10 * 1000 * 1000),
  Configuration(actors = 10 * 1000, hops = 10 * 1000 * 1000),
  Configuration(actors = 100 * 1000, hops = 10 * 1000 * 1000),
  Configuration(actors = 1000 * 1000, hops = 10 * 1000 * 1000),
  Configuration(actors = 10 * 1000 * 1000, hops = 10 * 1000 * 1000),

  Configuration(actors = 10, hops = 100 * 1000 * 1000),
  Configuration(actors = 1000, hops = 100 * 1000 * 1000),
  Configuration(actors = 10 * 1000, hops = 100 * 1000 * 1000),
  Configuration(actors = 100 * 1000, hops = 100 * 1000 * 1000),
  Configuration(actors = 1000 * 1000, hops = 100 * 1000 * 1000),
  Configuration(actors = 10 * 1000 * 1000, hops = 100 * 1000 * 1000),
]
