from functools import reduce
import measurement

class ProcessMeasure:
  @staticmethod
  def for_freebsd():
    import ps_list_freebsd
    import ps_measure_freebsd
    return ProcessMeasure(ps_list_freebsd, ps_measure_freebsd)

  @staticmethod
  def for_freebsd_psutil():
    import ps_list_psutil
    import ps_measure_psutil
    return ProcessMeasure(ps_list_psutil, ps_measure_psutil)

  def __init__(self, list_mod, measure_mod):
    self.list_fn = list_mod.list_recursive_child_processes_of
    self.measure_fn = measure_mod.measure

  def measure_recursive(self, pid):
    all_pids = self.list_fn(pid)
    if len(all_pids) > 0:
      return reduce(measurement.accumulate, map(lambda pid: self.measure_fn(pid), all_pids), None)
    else:
      return None
