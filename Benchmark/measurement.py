class Measurement:
  def __init__(self, cpu_time, user_time, system_time, elapsed_time, cpu_usage, vss_mib, rss_mib, num_processes, taken_at):
    self.cpu_time = cpu_time
    self.user_time = user_time
    self.system_time = system_time
    self.elapsed_time = elapsed_time
    self.cpu_usage = cpu_usage
    self.vss_mib = vss_mib
    self.rss_mib = rss_mib
    self.num_processes = num_processes
    self.taken_at = taken_at

  def as_dict(self):
      return {'cpu_time': self.cpu_time,
              'user_time': self.user_time,
              'system_time': self.system_time,
              'elapsed_time': self.elapsed_time,
              'cpu_usage': self.cpu_usage,
              'vss_mib': self.vss_mib,
              'rss_mib': self.rss_mib,
              'num_processes': self.num_processes,
              'taken_at': self.taken_at}

  def __repr__(self):
    return 'Measurement %s' % (self.as_dict())

def accumulate(a, b):
  if a == None:
    return b
  if b == None:
    return None 
  return Measurement(
    cpu_time = a.cpu_time + b.cpu_time,
    user_time = a.user_time + b.user_time,
    system_time = a.system_time + b.system_time,
    elapsed_time = max(a.elapsed_time, b.elapsed_time),
    cpu_usage = a.cpu_usage + b.cpu_usage,
    vss_mib = a.vss_mib + b.vss_mib,
    rss_mib = a.rss_mib + b.rss_mib,
    num_processes = a.num_processes + b.num_processes,
    taken_at = a.taken_at + b.taken_at)

