import psutil
import process_util
from collections import OrderedDict

def list_recursive_child_processes_of(pid):
  try:
    p = psutil.Process(pid.pid)
    return list(map(process_util.Pid, __uniq([p.pid] + list(map(lambda c: c.pid, p.children(recursive = True))))))
  except psutil.NoSuchProcess:
    return []

def __uniq(pids):
  return list(OrderedDict.fromkeys(pids))
