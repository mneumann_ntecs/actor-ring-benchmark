import process_util

def list_recursive_child_processes_of(pid):
  pids = []
  __visit_recursive_child_processes_of(pid, pids.append)
  return pids

def __child_processes_of(pid):
  return map(process_util.Pid, map(int, process_util.execute_cmd('pgrep -P %s' % (pid)).split()))

def __visit_recursive_child_processes_of(pid, visit):
  visit(pid)
  for child_pid in __child_processes_of(pid):
    __visit_recursive_child_processes_of(child_pid, visit)
