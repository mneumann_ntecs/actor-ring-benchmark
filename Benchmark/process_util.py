import measurement
import os

def execute_cmd(cmd):
  stream = os.popen(cmd)
  output = stream.read()
  return output

class Pid:
  def __init__(self, pid):
    self.pid = pid

  def __repr__(self):
    return "Pid(%s)" % (self.pid)

  def as_int(self):
    return self.pid

  def __str__(self):
    return str(self.pid)
