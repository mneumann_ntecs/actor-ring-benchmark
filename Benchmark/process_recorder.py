import threading
import time

class ProcessRecorder:
  def __init__(self, process_measure, pid, recorder, interval = 0.1):
    self.process_measure = process_measure
    self.pid = pid
    self.recorder = recorder
    self.interval = interval
    self.thread = threading.Thread(target=lambda process_recorder: process_recorder._loop(), args=(self, ))
    self.runnable = threading.Event()
    self.runnable.set()

  def start(self):
    self.thread.start()

  def stop(self):
    self.runnable.clear()
    self.thread.join()

  def _loop(self):
    while self.runnable.isSet():
      total = self.process_measure.measure_recursive(self.pid)
      if total != None:
        self.recorder.record_entry(total)
      time.sleep(self.interval)

