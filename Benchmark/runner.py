import benchmarks
import configurations
import process_recorder
import process_util
import process_measure
import subprocess
import os
import time
import platform
import json
import random
import sys
import fmt_utils 

def get_git_revision():
  return process_util.execute_cmd("git rev-parse HEAD").rstrip()

class BenchmarkMeasurement:
  def __init__(self, benchmark, configuration, git_revision):
    self.benchmark = benchmark
    self.configuration = configuration
    self.records = []
    self.started_at = None
    self.stopped_at = None
    self.total_runtime = None
    self.uname = platform.uname()
    self.git_revision = git_revision

    self.stdout_lines = None
    self.stderr_lines = None
    self.returncode = None

  def as_dict(self):
    return {
      'benchmark': {
        'name': self.benchmark.name,
        'title': self.benchmark.title,
        'path': self.benchmark.path,
        'command': self.benchmark.command(self.configuration),
        'lang': self.benchmark.lang,
      },
     'git_revision': self.git_revision,
     'configuration': self.configuration.as_dict(),
     'platform': {
       'system': self.uname.system,
       'node': self.uname.node,
       'release': self.uname.release,
       'version': self.uname.version,
       'machine': self.uname.machine,
       'processor': self.uname.processor},
     'process': {
       'started_at': self.started_at,
       'stopped_at': self.stopped_at,
       'total_runtime': self.total_runtime,
       'stdout_lines': self.stdout_lines,
       'stderr_lines': self.stderr_lines,
       'returncode': self.returncode},
     'measurements': list(map(lambda x: x.as_dict(), self.records)) }

  def start(self):
    self.started_at = time.time()

  def stop(self):
    self.stopped_at = time.time()
    self.total_runtime = self.stopped_at - self.started_at

  def record_entry(self, measurement):
    self.records.append(measurement)

class LogfileRepository:
  def __init__(self, logdir):
    self.logdir = logdir

  def filename_for(self, benchmark_measurement):
    return "{:s}_{:s}_{:s}_{:s}_{:s}_T{:d}.json".format(
      benchmark_measurement.benchmark.name,
      fmt_utils.num_to_human(benchmark_measurement.configuration.actors),
      fmt_utils.num_to_human(benchmark_measurement.configuration.hops),
      benchmark_measurement.uname.system.lower(),
      benchmark_measurement.uname.machine.lower(),
      int(benchmark_measurement.started_at))

  def put(self, benchmark_measurement):
    path = os.path.join(self.logdir, self.filename_for(benchmark_measurement))
    with open(path, 'x') as f:
      json.dump(benchmark_measurement.as_dict(), f, indent=2)

class BenchmarkRunner:
  def __init__(self, git_revision):
    self.git_revision = git_revision
    self.process_measure = process_measure.ProcessMeasure.for_freebsd_psutil()

  def run(self, benchmark, configuration):
    assert(benchmark.supports_configuration(configuration))
    bm_measurement = BenchmarkMeasurement(benchmark, configuration, self.git_revision)

    cwd = os.path.join("..", "Implementations", *benchmark.path)
    cmd = benchmark.command(configuration)
    #print("| Starting benchmark", benchmark.name, "with", configuration, "in cwd", cwd, "cmd", cmd)

    bm_measurement.start()

    with subprocess.Popen(cmd, cwd = cwd, stdin = subprocess.DEVNULL, stdout = subprocess.PIPE, stderr = subprocess.PIPE) as proc:
      ps_recorder = process_recorder.ProcessRecorder(
        process_measure = self.process_measure,
        pid = process_util.Pid(proc.pid),
        recorder = bm_measurement,
        interval = 0.1)
      ps_recorder.start()

      outs, errs = proc.communicate()

      bm_measurement.stdout_lines = outs.decode('utf-8').splitlines()
      bm_measurement.stderr_lines = errs.decode('utf-8').splitlines()
      bm_measurement.returncode = proc.returncode
      bm_measurement.stop()

      ps_recorder.stop()

      if proc.returncode != 0:
        print("FAILED")

    return bm_measurement

def all_combinations():
  return [(bm, conf) for bm in benchmarks.ALL for conf in configurations.ALL if bm.supports_configuration(conf)]

def include_all(x):
  return True

def main(recovery_time = 2, filter_benchmarks = include_all, filter_configurations = include_all):
  git_revision = get_git_revision()
  started = time.time()
  logfile_repo = LogfileRepository("./Logs")

  combinations = all_combinations()
  combinations = filter(lambda t: filter_benchmarks(t[0]), combinations)
  combinations = filter(lambda t: filter_configurations(t[1]), combinations)
  combinations = list(combinations)
  random.shuffle(combinations)

  for idx, (bm, conf) in enumerate(combinations):
    current_elapsed = time.time() - started
    log = "[{:04d}/{:04d} @{:4d}s] {:>30s} {:>9d} {:>12d}".format(
      idx,
      len(combinations) - 1,
      int(current_elapsed),
      bm.name,
      conf.actors,
      conf.hops)
    print(log)
    bm_measurement = BenchmarkRunner(git_revision).run(bm, conf)
    logfile_repo.put(bm_measurement)

    # Recovery time between benchmark runs
    time.sleep(recovery_time)

  ended = time.time()
  print("Benchmark took", ended - started)

main()

#main(
#  filter_benchmarks = lambda bm: bm.name == 'erlang-default',
#  filter_configurations = lambda cfg: cfg.actors == 1000 and cfg.hops < 100000)
